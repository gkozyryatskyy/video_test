package com.test.services;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.test.model.Status;
import com.test.model.Task;
import com.test.model.Type;
import com.test.model.Video;
import com.test.util.VideoUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.eventbus.ReplyFailure;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;

@Path("/videos")
@Api(value = "VideoService")
public class VideoService extends Service {

    private static final ConcurrentHashMap<String, Task> tasksMap = new ConcurrentHashMap<>();
    // do we need to generate tasks on startup for all videos?
    // static {
    // tasksMap = new ConcurrentHashMap<>(Arrays.stream(VideoUtils.VIDEOS_FOLDER.listFiles()).filter(f -> f.isFile())
    // .map(v -> new Task(v.getName(), Type.VIDEO, Status.DONE))
    // .collect(Collectors.toMap(e -> e.getId(), e -> e)));
    // }

    public VideoService(RoutingContext context) {
        super(context);
    }

    @Path("/")
    @ApiOperation(httpMethod = "GET", value = "videos", notes = "get all videos list", response = Video.class, responseContainer = "List")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success") })
    public void videos() {
        log.info("Videos");
        respond(Arrays.stream(VideoUtils.VIDEOS_FOLDER.listFiles()).filter(f -> f.isFile()).map(v -> {
            String name = v.getName();
            return new Video(name, tasksMap.get(name));
        }).collect(Collectors.toList()));
    }

    @Path("/{id}")
    @ApiOperation(httpMethod = "GET", value = "get", notes = "Get video by id", response = Video.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Video not found.") })
    public void get(@PathParam("id") String id) {
        log.info("Get [{}]", id);
        File file = new File(VideoUtils.VIDEOS_FOLDER, id);
        if (file.isFile()) {
            String name = file.getName();
            respond(new Video(name, tasksMap.get(name)));
        } else {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 404, "Video [" + id + "] not found.");
        }
    }

    @Path("/{id}")
    @ApiOperation(httpMethod = "DELETE", value = "get", notes = "Delete video by id")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Video not found.") })
    public void delete(@PathParam("id") String id) {
        log.info("Delete [{}]", id);
        File file = new File(VideoUtils.VIDEOS_FOLDER, id);
        if (file.isFile()) {
            file.delete();
            respond();
        } else {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 404, "Video [" + id + "] not found.");
        }
    }

    @Path("/{id}/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(httpMethod = "POST", value = "upload", notes = "Upload video")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 409, message = "Video already exists.") })
    @ApiImplicitParams(@ApiImplicitParam(required = true, dataType = "file", name = "upload", paramType = "form"))
    public void upload(@PathParam("id") String id, @ApiParam(hidden = true) FileUpload upload) {
        log.info("Upload [{}]", id);
        File file = new File(VideoUtils.VIDEOS_FOLDER, id);
        if (!file.exists()) {
            new File(upload.uploadedFileName()).renameTo(file);
            respond();
        } else {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 409,
                    "Video file with id [" + id + "] already exists.");
        }
    }

    private File getOperationableFile(String id) {
        File file = new File(VideoUtils.VIDEOS_FOLDER, id);
        if (!file.isFile()) {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 404, "Video [" + id + "] not found.");
        }
        Task task = tasksMap.get(id);
        if (task == null || Status.DONE.equals(task.getStatus())) {
            return file;
        } else {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 406, "Task for video [" + id + "] is not done.");
        }
    }

    @Path("/{id}/download")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(httpMethod = "GET", value = "download", notes = "Download video by id")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Video not found."),
            @ApiResponse(code = 406, message = "Wrong video state.") })
    public void download(@PathParam("id") String id) {
        log.info("Download [{}]", id);
        File file = getOperationableFile(id);
        log.info("Start downloading [{}]", id);
        HttpServerResponse response = context.response();
        response.setChunked(true);
        response.putHeader(HttpHeaders.CONTENT_TYPE, "application/octet-stream;charset=UTF-8");
        response.putHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.sendFile(file.getPath(), res -> log.info("Complete downloading [{}]", id));
    }

    @Path("/{id}/merge")
    @ApiOperation(httpMethod = "GET", value = "merge", notes = "Merge videos", response = String.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Video not found."),
            @ApiResponse(code = 406, message = "Wrong video state.") })
    public void merge(@PathParam("id") String id,
            @QueryParam("innerVideoSounceId") @ApiParam(required = true) String innerVideoSounceId,
            @QueryParam("secondPointer") @ApiParam(required = true) int secondPointer,
            @QueryParam("destVideoId") @ApiParam(required = true) String destVideoId) {
        log.info("Merge [{}], [{}] on {} second into [{}]", id, innerVideoSounceId, secondPointer, destVideoId);
        File mainSource = getOperationableFile(id);
        File innerSounse = getOperationableFile(innerVideoSounceId);
        File destFile = new File(VideoUtils.VIDEOS_FOLDER, destVideoId);
        if (destFile.exists()) {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 409,
                    "Video file with id [" + destVideoId + "] already exists.");
        }
        log.info("Start merging [{}]", destVideoId);
        context.vertx().<Void> executeBlocking(future -> {
            try {
                tasksMap.put(destVideoId, new Task(destVideoId, Type.MERGE, Status.START));
                VideoUtils.mergeVideos(mainSource, innerSounse, secondPointer,
                        new File(VideoUtils.VIDEOS_FOLDER, destVideoId));
                future.complete();
            } catch (java.lang.Exception e) {
                log.error(e.getMessage(), e);
                future.fail(e);
            }
        }, false, res -> {
            log.info("End merging [{}]", destVideoId);
            if (res.succeeded()) {
                tasksMap.get(destVideoId).setStatus(Status.DONE);
            } else {
                tasksMap.get(destVideoId).setStatus(Status.ERROR);
            }
        });
        respond(destVideoId);
    }

    @Path("/{id}/preview")
    @ApiOperation(httpMethod = "GET", value = "preview", notes = "Evenly take 100 images from video and make a preview video from them.", response = String.class)
    @ApiResponses({ @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404, message = "Video not found."),
            @ApiResponse(code = 406, message = "Wrong video state.") })
    public void preview(@PathParam("id") String id,
            @QueryParam("destVideoId") @ApiParam(required = true) String destVideoId) {
        log.info("Preview [{}] into [{}]", id, destVideoId);
        File mainSource = getOperationableFile(id);
        File destFile = new File(VideoUtils.VIDEOS_FOLDER, destVideoId);
        if (destFile.exists()) {
            throw new ReplyException(ReplyFailure.RECIPIENT_FAILURE, 409,
                    "Video file with id [" + destVideoId + "] already exists.");
        }
        log.info("Start previewing [{}]", destVideoId);
        context.vertx().<Void> executeBlocking(future -> {
            try {
                tasksMap.put(destVideoId, new Task(destVideoId, Type.PREVIEW, Status.START));
                VideoUtils.videoPreview(mainSource, new File(VideoUtils.VIDEOS_FOLDER, destVideoId));
                future.complete();
            } catch (java.lang.Exception e) {
                log.error(e.getMessage(), e);
                future.fail(e);
            }
        }, false, res -> {
            log.info("End previewing [{}]", destVideoId);
            if (res.succeeded()) {
                tasksMap.get(destVideoId).setStatus(Status.DONE);
            } else {
                tasksMap.get(destVideoId).setStatus(Status.ERROR);
            }
        });
        respond(destVideoId);
    }

}
