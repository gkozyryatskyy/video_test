package com.test.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.core.WebApi;

import io.vertx.ext.web.RoutingContext;

public class Service {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    protected RoutingContext context;

    public Service(RoutingContext context) {
        this.context = context;
    }

    public void respond() {
        WebApi.respond(context);
    }

    public void respond(Object o) {
        WebApi.respond(context, o);
    }

}
