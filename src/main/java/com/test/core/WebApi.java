package com.test.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.test.services.VideoService;
import com.test.util.JsonUtils;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class WebApi extends AbstractVerticle {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    protected HttpServer server;

    @Override
    public void start() throws Exception {
        server = vertx.createHttpServer(new HttpServerOptions().setIdleTimeout(30));
        Router router = Router.router(vertx);
        addRouts(router);
        router.route("/api-docs/*").handler(StaticHandler.create());
        server.requestHandler(router::accept).listen(8080, "localhost");
    }

    @Override
    public void stop() {
        server.close();
    }

    private void addRouts(Router router) {
        router.route().handler(BodyHandler.create());
        router.route().handler(c -> {
            c.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
            c.next();
        });
        router.route().failureHandler(this::handleException);
        addVideoApiRouts(router);
    }

    private void addVideoApiRouts(Router router) {
        router.get("/api/videos").handler(c -> new VideoService(c).videos());
        router.get("/api/videos/:id").handler(c -> new VideoService(c).get(c.pathParam("id")));
        router.delete("/api/videos/:id").handler(c -> new VideoService(c).delete(c.pathParam("id")));
        router.post("/api/videos/:id/upload")
                .handler(c -> new VideoService(c).upload(c.pathParam("id"), c.fileUploads().stream().findAny().get()));
        router.get("/api/videos/:id/download").handler(c -> new VideoService(c).download(c.pathParam("id")));
        router.get("/api/videos/:id/merge")
                .handler(c -> new VideoService(c).merge(c.pathParam("id"), c.request().getParam("innerVideoSounceId"),
                        Integer.parseInt(c.request().getParam("secondPointer")), c.request().getParam("destVideoId")));
        router.get("/api/videos/:id/preview")
                .handler(c -> new VideoService(c).preview(c.pathParam("id"), c.request().getParam("destVideoId")));
    }

    private void handleException(RoutingContext context) {
        Throwable cause = context.failure();
        log.error("Handler exception. ", cause);
        if (cause instanceof ReplyException) {
            ReplyException ex = (ReplyException) cause;
            respondError(context, ex.failureCode(), ex.getMessage());
        } else if (cause instanceof RuntimeException) {
            respondError(context, 400, cause.getMessage());
        } else {
            respondError(context, 500, cause.getMessage());
        }
    }

    public static void respond(RoutingContext context) {
        context.response().end();
    }

    public static void respond(RoutingContext context, Object o) {
        context.response().end(JsonUtils.writeValueAsString(o));
    }

    public static void respondError(RoutingContext context, int status, String message) {
        if (message != null) {
            context.response().setStatusCode(status).end("{\"error\":\"" + message + "\"}");
        } else {
            context.response().setStatusCode(status).end();
        }
    }
}
