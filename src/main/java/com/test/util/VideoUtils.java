package com.test.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;

public class VideoUtils {

    public static final int PREVIEW_PARTS = 100;
    public static final File VIDEOS_FOLDER = new File("videos");
    static {
        if (!VIDEOS_FOLDER.exists()) {
            if (!VIDEOS_FOLDER.mkdir()) {
                throw new IllegalStateException("Can`t create [videos] directory.");
            }
            // can`t pack sample videos in jar file it corrupts video files
            // else {
            // try {
            // copyToFolder("h264.mp4", "h264.mp4");
            // } catch (RuntimeException e) {
            // }
            // try {
            // copyToFolder("xvid.avi", "xvid.avi");
            // } catch (RuntimeException e) {
            // }
            // }
        }
    }

    public static void copyToFolder(String source, String distName) {
        try (InputStream in = ClassLoader.getSystemResourceAsStream(source)) {
            Files.copy(in, new File(VIDEOS_FOLDER, distName).toPath());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void mergeVideos(File mainVideoFile, File secondVideoFile, int secondPointer, File outVideoPath)
            throws Exception, org.bytedeco.javacv.FrameRecorder.Exception {
        FFmpegFrameGrabber mainVideo = new FFmpegFrameGrabber(mainVideoFile);
        mainVideo.start();
        long videoLength = mainVideo.getLengthInTime() / 1000000;
        if (videoLength < secondPointer) {
            throw new IllegalArgumentException("Video length = " + videoLength
                    + " seconds. It is more then pointer value = " + secondPointer + " second.");
        }

        FFmpegFrameGrabber secondVideo = new FFmpegFrameGrabber(secondVideoFile);
        secondVideo.start();

        FFmpegFrameRecorder rec = new FFmpegFrameRecorder(outVideoPath, mainVideo.getImageWidth(),
                mainVideo.getImageHeight(), mainVideo.getAudioChannels());
        rec.setFrameRate(mainVideo.getFrameRate());
        rec.setSampleFormat(mainVideo.getSampleFormat());
        rec.setSampleRate(mainVideo.getSampleRate());
        rec.setVideoCodec(mainVideo.getVideoCodec());
        rec.start();

        Frame frame;
        long nanosPointer = secondPointer * 1000000;
        // first part from first video
        while (mainVideo.getTimestamp() < nanosPointer) {
            frame = mainVideo.grab();
            rec.record(frame);
        }
        // second video
        while ((frame = secondVideo.grab()) != null) {
            rec.record(frame);
        }
        // second part from first video
        while ((frame = mainVideo.grab()) != null) {
            rec.record(frame);
        }
        mainVideo.stop();
        secondVideo.stop();
        rec.stop();
    }

    public static void videoPreview(File mainVideoFile, File outVideoPath)
            throws Exception, org.bytedeco.javacv.FrameRecorder.Exception {
        FFmpegFrameGrabber mainVideo = new FFmpegFrameGrabber(mainVideoFile);
        mainVideo.start();
        FFmpegFrameRecorder rec = new FFmpegFrameRecorder(outVideoPath, mainVideo.getImageWidth(),
                mainVideo.getImageHeight(), mainVideo.getAudioChannels());
        rec.setVideoCodec(mainVideo.getVideoCodec());
        rec.start();
        int frames = mainVideo.getLengthInFrames() / PREVIEW_PARTS;
        for (int i = 1; i <= PREVIEW_PARTS; i++) {
            mainVideo.setFrameNumber(frames * i);
            rec.record(mainVideo.grabImage());
        }
        mainVideo.stop();
        rec.stop();
    }

}
