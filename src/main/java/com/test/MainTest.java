package com.test;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FrameGrabber.Exception;

import com.test.util.VideoUtils;

import org.bytedeco.javacv.OpenCVFrameConverter;

import static org.bytedeco.javacpp.opencv_imgcodecs.*;

import java.io.File;

public class MainTest {

    private static final OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();

    public static void main(String... args) {
        VideoUtils.VIDEOS_FOLDER.exists(); // init videos dir
    }

    public static void framesFromVideo(String path, int parts) throws Exception {
        System.out.println(path);
        System.out.println(new File(path).exists());
        FFmpegFrameGrabber video = new FFmpegFrameGrabber(path);
        video.start();

        int frames = video.getLengthInFrames() / parts;
        for (int i = 1; i <= parts; i++) {
            video.setFrameNumber(frames * i);
            System.out.println(video.getFrameNumber());
            IplImage image = converter.convert(video.grabImage());
            cvSaveImage("video-frame-" + i + ".png", image);
        }

        video.stop();
    }

}
