# Build
----------------------------------------------
> mvn clean install

# Run
----------------------------------------------
> java -jar target/test-1.0.jar

> **Please run it from project folder, because "videos" folder contains h264.mp4 and xvid.avi samples.**

# Api doc
----------------------------------------------

> http://localhost:8080/api-docs/

> **Unfortunately swagger [download](http://localhost:8080/api-docs/#!/VideoService/download) link  corrupting the file.** 
> **Please use download from the browser url or with** 
> `wget -O {id} http://localhost:8080/api/videos/{id}/download`

# Examples
tested on 

* OS X El Capitan
* Ubuntu 16.04 LTS
----------------------------------------------
> ## merge videos ##
`файл (видео), закодированый в H264 вовнутрь этого видео вставить кусок другого видео, закодированного в формате Xvid. При этом, звук должен сохраняться от первого видео, и не сдвигаться/теряться`

> *running merge task*

> `curl -XGET 'http://localhost:8080/api/videos/h264.mp4/merge?innerVideoSounceId=xvid.avi&secondPointer=5&destVideoId=merge_result.mp4'`

> *checking task status (You can skip this step. Feature for big video files that produses long running tasks)* 

> `curl -XGET 'http://localhost:8080/api/videos/merge_result.mp4'`

> *downloading video*

> `wget -O merge_result.mp4 http://localhost:8080/api/videos/merge_result.mp4/download`

> ## preview videos ##
`разрезать на небазовые слайды`

> I dont know \ can not find the definition of `небазовые слайды` instead of this I split video evenly on 100 images and make a **preview** video from them.

> *running preview tasks*

> `curl -XGET 'http://localhost:8080/api/videos/h264.mp4/preview?destVideoId=h264-pr.mp4'`

> `curl -XGET 'http://localhost:8080/api/videos/xvid.avi/preview?destVideoId=xvid-pr.avi'`

> *checking tasks status (You can skip this step. Feature for big video files that produses long running tasks)*

> `curl -XGET 'http://localhost:8080/api/videos/h264-pr.mp4'`

> `curl -XGET 'http://localhost:8080/api/videos/xvid-pr.avi'`

> *downloading videos*

> `wget -O h264-pr.mp4 http://localhost:8080/api/videos/h264-pr.mp4/download`

> `wget -O xvid-pr.avi http://localhost:8080/api/videos/xvid-pr.avi/download`

> ## You can also use [upload](http://localhost:8080/api-docs/#!/VideoService/upload) api for uploading you video files and test them. Swagger uploading files correctly. Enjoy =) ##